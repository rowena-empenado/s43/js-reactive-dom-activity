let posts = [];
let count = 1;

// Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
    e.preventDefault();

    posts.push({
        id: count,
        title: document.querySelector('#txt-title').value,
        body: document.querySelector('#txt-body').value
    })

    count++;

    showPosts(posts);
    alert('Successfully Added!');
})

const showPosts = (posts) => {

    let postEntries  = '';

    posts.forEach(post => {
        postEntries+= `
        
            <div id="post-${post.id}" style="border-style:solid; padding:1em; border-radius:5px; border-color:gray; margin-bottom: 1em; max-width: 50%;">
                <h3 id="post-title-${post.id}" style="color:violet;"> ${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')"> Edit </button>
                <button onClick="deletePost('${post.id}')"> Delete </button>
            </div>

        `
    });

    document.querySelector('#div-post-entries').innerHTML = postEntries;

}

// Edit a post

const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
    e.preventDefault();

    for(let i = 0; i < posts.length; i++) {

        if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value)  {

            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
            alert('Successfully Updated!');

            break;

        }
    }
})



// Delete a post --- Activity

function deletePost(id) {
    
    const index = posts.findIndex(object => {
        return object.id == id;
    });

    console.log(index)

    posts.splice(index, 1);

    document.querySelector(`#post-${id}`).remove();

    console.log(posts);

    alert('Successfully Deleted!');
}

